package Visual;

import grafo.NodoGas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.List;
import java.awt.MenuItem;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.omg.CORBA.Bounds;
import org.openstreetmap.gui.jmapviewer.*;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;
import org.openstreetmap.gui.jmapviewer.interfaces.TileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.BingAerialTileSource;
import org.openstreetmap.gui.jmapviewer.tilesources.OsmTileSource;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Interface {

	private JFrame frame;
	private JMapViewer map;
	private ArrayList<String> tiposDeNodos;
	public ArrayList<MapMarkerDot> marcadores;
	public ArrayList<MapPolygon> aristas;
	public ArrayList<Point> puntos;
	public ArrayList<Integer> valores;
	private Color colorNodo;
	public int flujoMaximo;

	public Interface() {
		inicializar();
	}

	public void inicializar() {

		crearFrame();
		crearMapa();
		crearBarraDeHerramientasSuperior();
		frame.setVisible(true);

	}

	private void crearFrame() {
		frame = new JFrame();
		frame.setBounds(0, 50, 2000, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public boolean esVisible() {
		return map.isVisible();
	}

	private void crearMapa() {
		tiposDeNodos = new ArrayList<String>();
		marcadores = new ArrayList<MapMarkerDot>();
		aristas = new ArrayList<MapPolygon>();
		puntos = new ArrayList<Point>();
		valores = new ArrayList<Integer>();
		flujoMaximo = 0;
		colorNodo = Color.WHITE;
		map = new JMapViewer();
		map.setDisplayPositionByLatLon(-34, -64, 5);
		map.setZoomContolsVisible(false);
		frame.getContentPane().add(map);
		crearNodoPorClick();

	}

	private void crearNodoPorClick() {
		map.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent click) {

				final Coordinate coordenada = map.getPosition(click.getPoint());
				final JFrame panelNodo = new JFrame();
				panelNodo.getContentPane().setLayout(new GridLayout(10, 0));
				JLabel nombreNodo = new JLabel("Ingrese nombre del Nodo");
				nombreNodo.setSize(100, 25);
				panelNodo.getContentPane().add(nombreNodo);

				final JTextField nombreN = new JTextField();
				panelNodo.getContentPane().add(nombreN);

				JLabel valor = new JLabel("Ingrese cantidad de gas");
				panelNodo.getContentPane().add(valor);

				final JTextField ingresarValor = new JTextField();
				panelNodo.getContentPane().add(ingresarValor);

				panelNodo.setBounds(click.getX(), click.getY(), 200, 250);
				panelNodo.setVisible(true);

				JComboBox<String> tipos = new JComboBox<String>();

				tipos.addItem("Seleccione tipo de nodo");
				tipos.addItem("De paso");
				tipos.addItem("Productor");
				tipos.addItem("Consumidor");

				tipos.addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent e) {
						if (e.getItem().equals("De paso")) {
							colorNodo = Color.GREEN;
						}
						if (e.getItem().equals("Consumidor")) {
							colorNodo = Color.RED;
						}
						if (e.getItem().equals("Productor")) {
							colorNodo = Color.BLUE;
						}
					}

				});
				panelNodo.getContentPane().add(tipos);

				JButton agregar = new JButton("Agregar");
				panelNodo.getContentPane().add(agregar);
				agregar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						MapMarkerDot nuevo = new MapMarkerDot(coordenada);
						nuevo.setName(nombreN.getText()+ingresarValor.getText());
						nuevo.setBackColor(colorNodo);
						String valor = ingresarValor.getText();
						valores.add(Integer.parseInt(valor));
						marcadores.add(nuevo);
						map.addMapMarker(nuevo);
						map.setMapMarkerVisible(true);
						panelNodo.setVisible(false);
					}
				});

			}

		});
	}

	private void crearBarraDeHerramientasSuperior() {
		final JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);

		JComboBox<Object> selectorTipoFuenteGUIMapa = new JComboBox<Object>(
				new TileSource[] { new OsmTileSource.Mapnik(),
						new OsmTileSource.CycleMap(),
						new BingAerialTileSource() });

		selectorTipoFuenteGUIMapa.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				map.setTileSource((TileSource) e.getItem());
			}
		});

		JLabel lblFuente = new JLabel("Tipo de Fuente");
		panel.add(lblFuente);
		panel.add(selectorTipoFuenteGUIMapa);

		// checkbox puntos en el mapa
		final JCheckBox mostrarNodos = new JCheckBox("Mostrar nodos en el mapa");
		mostrarNodos.setSelected(map.getMapMarkersVisible());
		mostrarNodos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				map.setMapMarkerVisible(mostrarNodos.isSelected());
			}
		});
		panel.add(mostrarNodos);

		// checkbox mostrar controles zoom
		final JCheckBox showZoomControls = new JCheckBox(
				"Mostrar controles del zoom");
		showZoomControls.setSelected(map.getZoomContolsVisible());
		showZoomControls.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				map.setZoomContolsVisible(showZoomControls.isSelected());
			}
		});
		panel.add(showZoomControls);

		JButton centrarVistaANodos = new JButton("Centrar nodos");
		centrarVistaANodos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				map.setDisplayToFitMapMarkers();
			}
		});
		/****/
		panel.add(centrarVistaANodos);
		// JButton flujoEs=new JButton("El Flujo maximo es de: "+flujoMaximo);

		// panel.add(flujoEs);

		final JButton btnAgregararista = new JButton("Tunel");
		btnAgregararista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFrame panelTunel = new JFrame();
				panelTunel.getContentPane().setLayout(new GridLayout(10, 0));
				JLabel nombre1 = new JLabel("Ingrese Primer Nodo");
				nombre1.setSize(100, 25);
				panelTunel.getContentPane().add(nombre1);

				final JTextField ingresar1 = new JTextField();
				panelTunel.getContentPane().add(ingresar1);
				ingresar1.setBounds(0, 50, 100, 25);

				JLabel nombre2 = new JLabel("Ingrese Segundo Nodo");
				nombre1.setSize(100, 25);
				panelTunel.getContentPane().add(nombre2);

				final JTextField ingresar2 = new JTextField();
				panelTunel.getContentPane().add(ingresar2);
				ingresar1.setBounds(0, 50, 100, 25);

				JLabel valor = new JLabel("Ingrese cantidad de gas");
				panelTunel.getContentPane().add(valor);

				final JTextField ingresarValor = new JTextField();
				panelTunel.getContentPane().add(ingresarValor);

				panelTunel.setBounds(btnAgregararista.getX(),
						btnAgregararista.getY() + 150, 200, 250);
				panelTunel.setVisible(true);

				JButton agregar = new JButton("Agregar");
				panelTunel.getContentPane().add(agregar);
				agregar.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						MapMarkerDot nodo1 = buscarMarcador(ingresar1.getText());
						MapMarkerDot nodo2 = buscarMarcador(ingresar2.getText());
						Coordinate uno = nodo1.getCoordinate();
						Coordinate dos = nodo2.getCoordinate();
						MapPolygon tunel = new MapPolygonImpl(ingresarValor
								.getText(), uno, uno, dos);
						Point punto = new Point(marcadores.indexOf(nodo1),
								marcadores.indexOf(nodo2));
						puntos.add(punto);
						aristas.add(tunel);
						map.addMapPolygon(tunel);
						panelTunel.setVisible(false);

					}
				});

			}
		});
		panel.add(btnAgregararista);
		final JLabel nombre1 = new JLabel("flujo maximo=" + flujoMaximo);
		panel.add(nombre1);
		JButton btnFlujoMaximo = new JButton("Flujo Maximo");
		btnFlujoMaximo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nombre1.setText("flujo maximo=" + flujoMaximo);
				panel.updateUI();

				final JFrame flujo = new JFrame();
				JLabel nombre1 = new JLabel("n" + "flujo maximo=" + flujoMaximo);
				nombre1.setSize(0, 0);
				flujo.getContentPane().add(nombre1);
				flujo.setVisible(true);
			}
		});
		panel.add(btnFlujoMaximo);

	}

	public void recibirFlujoMaximo(int a) {
		flujoMaximo = a;
	}

	private MapMarkerDot buscarMarcador(String nombre) {
		for (int i = 0; i < marcadores.size(); i++) {
			if (nombre.equals(marcadores.get(i).getName())) {

				return marcadores.get(i);
			}
		}
		return null;
	}

	private void actListaTipos() {
		ArrayList<String> rem = new ArrayList<String>();
		if (marcadores.size() > 0) {
			for (int i = 0; i < marcadores.size(); i++) {
				if (marcadores.get(i).getBackColor().equals(Color.BLUE)) {
					rem.add("Productor");
				}
				if (marcadores.get(i).getBackColor().equals(Color.GREEN)) {
					rem.add("De paso");
				}
				if (marcadores.get(i).getBackColor().equals(Color.RED)) {
					rem.add("Consumidor");
				}
			}
		}
		tiposDeNodos = rem;
	}

	public ArrayList<NodoGas> enviarNodos() {
		actListaTipos();
		ArrayList<NodoGas> ret = new ArrayList<NodoGas>();
		if (tiposDeNodos.size() > 0) {
			for (int i = 0; i < tiposDeNodos.size(); i++) {
				String tipo = tiposDeNodos.get(i);
				String nombre = marcadores.get(i).getName();
				int valor = valores.get(i);
				NodoGas nuevo = new NodoGas(tipo, nombre, valor);
				ret.add(nuevo);
			}
		}
		return ret;
	}

	public ArrayList<String> enviarNombres() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < marcadores.size(); i++) {
			ret.add(marcadores.get(i).getName());
		}
		return ret;
	}

	public ArrayList<String> enviarTipos() {
		ArrayList<String> ret = new ArrayList<String>();
		for (int i = 0; i < tiposDeNodos.size(); i++) {
			ret.add(tiposDeNodos.get(i));
		}
		return ret;
	}

	public int[][] enviarAristas() {
		int[][] matriz = new int[marcadores.size()][marcadores.size()];
		for (int i = 0; i < aristas.size(); i++) {
			int capacidad = Integer.parseInt(aristas.get(i).getName());
			int indice1 = (int) puntos.get(i).getX();
			int indice2 = (int) puntos.get(i).getY();
			matriz[indice1][indice2] = capacidad;

		}
		return matriz;
	}

}