package main;

import Visual.Interface;
import grafo.Grafo;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grafo g=new Grafo();
		Interface i=new Interface();
		while(i.esVisible()){
			g.recibirAristas(i.enviarAristas());			
			g.recibirNodos(i.enviarNodos());
			i.recibirFlujoMaximo(g.flujoMax());
			
		}
	}		
}