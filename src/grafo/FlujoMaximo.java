package grafo;

import java.util.LinkedList;

public class FlujoMaximo {
	public int vertices;
	
	public FlujoMaximo( int vertices){
		this.vertices=vertices;
	}
	
	boolean bfs(int valorAristas[][], int origen, int destino, int padres[]){ 
        
        boolean visitados[] = new boolean[vertices];
        for(int i=0; i<vertices; ++i) 
            visitados[i]=false; 
        
        LinkedList<Integer> cola = new LinkedList<Integer>(); 
        cola.add(origen); 
        visitados[origen] = true; 
        padres[origen]=-1; 
       
        while (cola.size()!=0){ 
            int u = cola.poll(); 
  
            for (int v=0; v<vertices; v++){ 
                if (visitados[v]==false && valorAristas[u][v] > 0){ 
                    cola.add(v); 
                    padres[v] = u; 
                    visitados[v] = true; 
                } 
            } 
        } 
        return (visitados[destino] == true); 
    } 
     
    public int edKarp(int grafo[][], int origen, int destino){ 
        int u, v; 
        int grafoResidual[][] = new int[vertices][vertices]; 
  
        for (u = 0; u < vertices; u++) {
            for (v = 0; v < vertices; v++){ 
                grafoResidual[u][v] = grafo[u][v];
            }
        }
        // Este arreglo lo llena BFS para guardar el camino
        int padres[] = new int[vertices]; 
  
        int flujoMaximo = 0;

        while (bfs(grafoResidual, origen, destino, padres)) { 
            int flujoCamino = Integer.MAX_VALUE; 
            for (v=destino; v!=origen; v=padres[v]){ 
                u = padres[v]; 
                flujoCamino = Math.min(flujoCamino, grafoResidual[u][v]); 
            } 
  
            // actualiza las capacidades residuales de las aristas 
            // y las reversas a lo largo del camino 
            for (v=destino; v != origen; v=padres[v]){ 
                u = padres[v]; 
                grafoResidual[u][v] -= flujoCamino; 
                grafoResidual[v][u] += flujoCamino; 
            }            
            flujoMaximo += flujoCamino; 
        }        
        return flujoMaximo; 
    }
}