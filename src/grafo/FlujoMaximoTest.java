package grafo;

import org.junit.*;

public class FlujoMaximoTest {
//aca hace falta crear la funcion cargas como en el grafo test y utilizar un getter
//tambien hay que probar los casos borde 
	private Grafo g = new Grafo();
	private Grafo f = new Grafo();

	public void CargaNodo() {
		g.agregarNodo(new NodoGas("Productor", "1", 16));
		g.agregarNodo(new NodoGas("Productor", "2", 13));
		g.agregarNodo(new NodoGas("Consumidor", "3", 20));
		g.agregarNodo(new NodoGas("Consumidor", "4", 4));
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 12);
		g.agregarArista(1, 0, 4);
		g.agregarArista(1, 3, 14);
		g.agregarArista(2, 1, 9);
		g.agregarArista(3, 2, 7);		
	}
	
	@Test
	public void bfsTest(){
		CargaNodo();
		int padres []= new int [4];
		FlujoMaximo flujo=new FlujoMaximo(g.getAristas().length);
		Assert.assertTrue(flujo.bfs(g.getAristas(), 0, g.getAristas().length-1, padres));
	}
	
	@Test
	public void EdKarpTest(){
		CargaNodo();
		FlujoMaximo flujo=new FlujoMaximo(g.getAristas().length);
		Assert.assertEquals(14, flujo.edKarp(g.getAristas(), 0, g.getAristas().length-1));
	}
	
	@Test
	public void EdKarpVertices(){
		CargaNodo();
		FlujoMaximo flujo=new FlujoMaximo(g.getAristas().length);
		Assert.assertEquals(4, flujo.vertices);
	}
	
	@Test
	public void EdKarpTestVacio(){
		Grafo grafoVacio=new Grafo();
		Assert.assertEquals(0, grafoVacio.flujoMax());
	}
}