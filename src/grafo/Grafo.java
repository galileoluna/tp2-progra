package grafo;

import java.util.ArrayList;

public class Grafo
{
	public ArrayList<NodoGas> nodos;
	public int [][] _capacidadesAristas;
	
	public Grafo(){
		nodos = new ArrayList<NodoGas>();
		_capacidadesAristas=new int[0][0];	
	}
	
	public int[][] getAristas(){
		return _capacidadesAristas;
	}
	
	// Cantidad de vertices
	public int vertices(){
		return nodos.size();
	}
	
	// Manejo de aristas	
	public void agregarNodo(NodoGas nuevo){
		int[][] matAux=new int[_capacidadesAristas.length+1][_capacidadesAristas.length+1];
		for(int i=0;i<_capacidadesAristas.length;i++){
			for(int j=0;j<_capacidadesAristas.length;j++){
				matAux[i][j]=_capacidadesAristas[i][j];
			}
		}
		_capacidadesAristas=matAux;
		nodos.add(nuevo);
	}
	
	public void recibirAristas(int[][] matriz){
		_capacidadesAristas=matriz;
	}
	
	public void recibirNodos(ArrayList<NodoGas> nodosEntrantes){
		if(nodosEntrantes.size()>nodos.size()){
			nodos.add(nodosEntrantes.get(nodosEntrantes.size()-1));
		}
	}
	
	public void agregarArista(int vertice1, int vertice2, int capacidad){
		_capacidadesAristas[vertice1][vertice2]=capacidad;
	}
	
	public void eliminarArista(int i, int j){
		_capacidadesAristas[i][j]=0;
	}
	
	public int flujoMax(){
		int[][] matrizN=new int[_capacidadesAristas.length+2][_capacidadesAristas.length+2];
		for(int i=0;i<_capacidadesAristas.length;i++){
			for(int j=1;j<_capacidadesAristas.length;j++){
				matrizN[i+1][j+1]=_capacidadesAristas[i][j];
			}
		}
		
		for(int i=0;i<nodos.size();i++){
			if(nodos.get(i).getTipo().equals("Productor")){
				matrizN[0][i+1]=nodos.get(i).getCantidadGas();
			}
			if(nodos.get(i).getTipo().equals("Consumidor")){
				matrizN[i+1][matrizN.length-1]=nodos.get(i).getCantidadGas();
			}
		}
		
		FlujoMaximo flujo=new FlujoMaximo(matrizN.length);
		return flujo.edKarp(matrizN, 0, matrizN.length-1);	
	}
}