package grafo;

import java.util.Arrays;
import org.junit.*;


public class GrafoTest {
	private Grafo g = new Grafo();
		
	public void CargaNodo() {
		g.agregarNodo(new NodoGas("Productor", "1", 16));
		g.agregarNodo(new NodoGas("Productor", "2", 13));
		g.agregarNodo(new NodoGas("Consumidor", "3", 20));
		g.agregarNodo(new NodoGas("Consumidor", "4", 4));
		g.agregarArista(0, 1, 10);
		g.agregarArista(0, 2, 12);
		g.agregarArista(1, 0, 4);
		g.agregarArista(1, 3, 14);
		g.agregarArista(2, 1, 9);
		g.agregarArista(3, 2, 7);		
	}

	@Test
	public void FlujoMaximoTest() {
		CargaNodo();
		Assert.assertEquals(g.flujoMax(), 23);
	}

	@Test
	public void cantVerticesTest() {
		CargaNodo();	
		Assert.assertEquals(g.vertices(),4);
	}

	@Test
	public void recibirAristaTest() {
		CargaNodo();
		int matriz[][] = new int[4][4];
		matriz[0][1]=10;
		matriz[0][2]=12;
		matriz[1][0]=4;
		matriz[1][3]=14;
		matriz[2][1]=9;
		matriz[3][2]=7;
		
		Assert.assertArrayEquals(matriz, g._capacidadesAristas);
	}
	
	@Test
	public void recibirAristaFalse(){
		CargaNodo();
		int matriz1[][] = new int[4][4];
		matriz1[0][1]=111;
		matriz1[0][2]=12;
		matriz1[1][0]=4;
		matriz1[1][3]=14;
		matriz1[2][1]=9;
		matriz1[3][2]=7;
		
		Assert.assertFalse(Arrays.equals(matriz1, g._capacidadesAristas));				
	}
	
	@Test 
	public void agregarAristaTest(){
		CargaNodo();
		
	}
	
	@Test 
	public void eliminarAristaTest(){
		CargaNodo();
		g.eliminarArista(0, 1);
		Assert.assertEquals(0, g.getAristas()[0][1]);
	}
	
	@Test 
	public void existeNodoTest(){
		CargaNodo();
		Assert.assertNotNull(g.nodos.get(0));
	}
	
	@Test
	public void NodoNoNull(){
		CargaNodo();
		NodoGas nodo = new NodoGas("Productor", "1", 16);
		Assert.assertNotNull(g.nodos);
	}
	
	@Test 
	public void getAristaTest(){
		CargaNodo();
		Assert.assertNotEquals(0, g.getAristas()[0][1]);
	}
}