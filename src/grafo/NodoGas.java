package grafo;

public class NodoGas {
	
	private String tipo;
	private String Nombre;
	private int cantidadGas;
	
	public NodoGas(String tipo,String Nombre, int valor){
		this.tipo=tipo;
		this.Nombre=Nombre;
		cantidadGas=valor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int getCantidadGas() {
		return cantidadGas;
	}

	public void setCantidadGas(int cantidadGas) {
		this.cantidadGas = cantidadGas;
	}
}